import serial
import time

# 打开串口COM4，进行通信，四合一传感器,光照、湿度、二氧化碳、温度
ser = serial.Serial("/dev/ttyUSB1", 9600)
# 打开串口COM5，进行通信,氨气参数
serN = serial.Serial("/dev/ttyUSB0", 9600)
i=1

def getData():
    if ser.is_open and serN.is_open:
        # 四合一传感器的控制命令
        send_data = bytes.fromhex('01 03 00 00 00 04 44 09')
        # 氨气传感器的控制命令
        send_dataN = bytes.fromhex('01 03 00 00 00 01 84 0A')
        # 向四合一传感器发送命令
        ser.write(send_data)
        # 向氨气传感器发送命令
        serN.write(send_dataN)
        # sleep中的单位为秒,传感器在300ms内响应
        time.sleep(1)
        # 四合一传感器读到的长度
        len_return_data = ser.inWaiting()
        print(len_return_data)
        # 氨气传感器读到的长度
        len_return_dataN = serN.inWaiting()
        print(len_return_dataN)
        if len_return_data and len_return_dataN:
            # 四合一传感器读到的数据
            return_data = ser.read(len_return_data)
            # 氨气传感器读到的数据
            return_dataN = serN.read(len_return_dataN)
            # 四合一传感器转为十六进制
            str_return_data = str(return_data.hex())
            # 氨气传感器转为十六进制
            str_return_dataN = str(return_dataN.hex())

            serData={}
            # 数据转化部分
            # CO2data
            global i
            print(i)
            i+=1
            CO2data = (int(str_return_data[6:8], 16)) * 256 + int(str_return_data[8:10], 16)
            serData['co2']=CO2data
            print('二氧化碳：%s ppm' % CO2data)
            # a1.value=CO2data
            #a1[0]=CO2data
            # wendata
            if int(str_return_data[10:12], 16) < 127:
                wendata = (int(str_return_data[10:12], 16) * 256 + int(str_return_data[12:14], 16)) / 100
                print('温度：%s 度' % wendata)
            else:
                wendata = (((int(str_return_data[10:12], 16) & 0x7F)) * 256 + int(str_return_data[12:14], 16)) / 100
                print('温度：%s' % wendata)
            serData['wendu']=wendata
            #a1[1]=wendata
            # a2.value=wendata
            # 湿度
            shidudata = ((int(str_return_data[14:16], 16)) * 256 + int(str_return_data[16:18], 16)) / 100
            print('湿度：%s' % shidudata + '%RH')
            serData['shidu']=shidudata
            # a3.value=shidudata
            # 光照
            guangzhaodata = ((int(str_return_data[18:20], 16)) * 256 + int(str_return_data[20:22], 16)) * 4
            print('光照：%s lx' % guangzhaodata)
            serData['guangzhao']=guangzhaodata
            # a4.value=guangzhaodata
            # 氨气
            NH3data = int(str_return_dataN[6:8], 16) + int(str_return_dataN[8:10], 16)
            print('氨气：%s ppm' % NH3data)
            serData['NH3']=NH3data
            # a5.value=NH3data
            return serData

